# read-multiple-files

Lecturas asíncronas de múltiples archivos controladas por medio del método Promises.all. Dicho método retorna una Promise que se cumplirá cuando todas las promesas del argumento iterable hayan sido cumplidas, o bien se rechazará cuando alguna de ellas se rechace.

## Conceptos a desarrollar

Promises

## Tecnologias

Lenguaje: JavaScript

## Uso

Para su uso abrir el archivo index.html en cualquier navegador