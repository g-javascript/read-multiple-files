contents = [];
promises = [];

function readFiles(event) {
    if (window.File && window.FileList && window.Blob && window.FileReader) {
        let list = document.getElementById('list');
        list.innerHTML += '<li>La API de archivos es soportada por el navegador</li>';
        var filesAmount = event.target.files.length;
        for (let i = 0; i < filesAmount; i++) {
            let fileName = event.target.files[i].name;
            list.innerHTML += '<li>Paso 1) Inicio de lectura del archivo ' + fileName + '</li>';
            this.promises.push(new Promise(resolve => {
                var reader = new FileReader();
                reader.onloadend = eventReader => {
                    list.innerHTML += '<li>Paso 2) Finaliza lectura de contenido del archivo ' + fileName + '</li>';
                    this.contents.push(eventReader.target.result);
                    resolve(true);
                }
                reader.readAsText(event.target.files[i]);
            }));
        }
        Promise.all(this.promises).then(response => {
            list.innerHTML += '<li>Paso 3) - Promesas concluidas. EL contenido de los archivos se encuentra cargado en la variable contents</li>';
        });
    } else {
        list.innerHTML = '<li>La API de archivos no está totalmente soportada en este navegador</li>';
    }
}